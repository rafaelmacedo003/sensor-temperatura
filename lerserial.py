#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

from datetime import datetime
import time
import json
import serial
import paho.mqtt.client as mqtt
client = mqtt.Client()
client.connect("test.mosquitto.org",1883)

# Iniciando conexao serial
comport = serial.Serial('COM3', 9600)

while 1 < 2:
	
	try:
		VALUE_SERIAL=comport.readline().replace('\n', '').replace('\r', '')
		
		today = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
		str = "{\"Temperature\":"+VALUE_SERIAL+",\"Date\":\""+today+"\"}"
		print str
		client.publish("iot-univem/sub-zero/temperatura", str)
		str = json.dumps({'value':float(VALUE_SERIAL),'timestamp':today})
		client.publish("iot-univem/sub-zero/temperatura", str)
	except Exception:
		print ("ERRO LEITURA")
		
	time.sleep(5)

# Fechando conexao serial
comport.close()
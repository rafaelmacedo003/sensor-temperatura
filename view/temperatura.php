    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Sensor de Temperatura
                    </h1>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div id="alert_temp" class=""></div>
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Gŕafico de Temperatura</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="flot-chart">
                                        <div class="flot-chart-content" id="flot-line-chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <script>
                $(document).ready(function(){
                    $("#btn_buscar").on("click",function(){
                        $.ajax({
                            url: "http://172.28.8.195:8080/temp",
                            method: "GET",
                            dateType: 'jsonp'
                            // data: {
                                // cep: cep
                            // }
                        }).done(function(result){

                            result = JSON.parse(result);


                        });
                    });

                    var offset = 0;

                    function update(){
                        plot();
                        setTimeout(update, 900);
                    }
                    update();

                    function plot() {
                        $.ajax({
                            url: "http://172.28.8.195:8080/temp",
                            method: "GET",
                            dateType: 'jsonp'
                        }).done(function(result){
                            /*
                                Tempo , Temperatura
                            */
                            // var sin = [
                            //     [new Date("2016-08-20T16:56").getTime(),0],
                            //     [new Date("2016-08-20T17:11").getTime(),5],
                            //     [new Date("2016-08-20T17:13").getTime(),10],
                            //     [new Date("2016-08-20T17:15").getTime(),20],
                            //     [new Date("2016-08-20T17:18").getTime(),-20.0],
                            //     [new Date("2016-08-20T17:20").getTime(),30],
                            //     [new Date("2016-08-20T17:22").getTime(),-15]
                            // ];

                            var data = [];
                            // result.length

                            $.each(result, function(key, value){
                                data.push([new Date(value.date).getTime(), value.value]);
                                if(value.value > 30){
                                    $("#alert_temp").addClass("alert alert-danger").html("<h4><b>FUDEUUUU!!! FUDEUUUU!!!!</b></h4>");
                                }else if(value.value > 28){
                                    $("#alert_temp").removeClass("alert alert-danger");
                                    $("#alert_temp").addClass("alert alert-warning").html("<h4><b>SORVETE COMEÇANDO A DERRETER</b></h4>");
                                }else{
                                    $("#alert_temp").removeClass("alert alert-danger");
                                    $("#alert_temp").addClass("alert alert-success").html("<h4><b>ESTA DE BOA</b></h4>");
                                }
                            });

                            var options = {
                                series: {
                                    lines: {
                                        show: true
                                    },
                                    points: {
                                        show: true
                                    }
                                },
                                lines: {
                                    show: true
                                },
                                points: {
                                    show: true
                                },
                                // grid: {
                                //     hoverable: true //IMPORTANT! this is needed for tooltip to work
                                // },
                                yaxis: {
                                    min: 20.0,
                                    max: 50.0
                                },
                                xaxis: {
                                    mode: 'time',
                                    minTickSize: [1, 'min'],
                                    min: new Date('2016-08-20T19:00').getTime(),
                                    max: new Date('2016-08-20T19:30').getTime(),
                                    timeformat: '%m/%d %H:%M'
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "'%s' of %x.1 is %y.4",
                                    shifts: {
                                        x: -100,
                                        y: 25
                                    }
                                }
                            };

                            var plotObj = $.plot($("#flot-line-chart"), [{
                                    data: data,
                                    label: "ºC"
                                }],
                                options);
                        });
                    }
                });
            </script>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Subzero - Sensor de Temperatura abaixo de zero</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="sensor_temperatura.php"><i class="fa fa-fw fa-dashboard"></i>Temperatura</a>
                    </li>
                    <li class="active">
                        <div id="rodape">
                            <img src="resources/sub_zero.gif" style="width:150%; height:250%; float:right">
                        </div>
                    </li>
                </ul>
            </div>
